#!/bin/bash

basedir=$( cd "$(dirname "$0")" ; pwd -P )

input="input"
mkdir output

echo "Unzipping files..."

for file in $input/*tar.gz
do
    tar xzvf $file --directory $input/
    rm -f $file
done

obs_path=$(ls $input/*rdn*/*obs_ort)
if [[ -f $obs_path ]]
then
    echo "Found obs file $obs_path"
else
    echo "Couldn't find obs file, exiting..."
    exit 1
fi

rfl_path=$(ls $input/*rfl*/*corr*img)
if [[ -f $rfl_path ]]
then
    echo "Found reflectance file $rfl_path"
else
    echo "Couldn't find reflectance file, exiting..."
    exit 1
fi

prefix=""
obs_file=$(basename "$obs_path")
if [[ $obs_file == f* ]]
then
    prefix=`echo $obs_file | cut -c1-16`
elif [[ $obs_file == ang* ]]
then
    prefix=`echo $obs_file | cut -c1-18`
fi

echo "Found prefix $prefix"

export PYTHONPATH="$basedir:$PYTHONPATH"
echo "PYTHONPATH updated to include base directory $basedir"

topo_coeffs_suffix="_topo_coeffs.json"
topo_coeffs_path="output/$prefix$topo_coeffs_suffix"
brdf_coeffs_prefix="output/$prefix"

topo_brdf_coeffs_cmd="python $basedir/command_line_tools/topo_brdf_coeffs.py  --img $rfl_path  --obs $obs_path  --od output/  --pref $prefix  --kernels sparse thick  --mask  --mask_threshold 0.3 0.7  --brdf  --topo"

image_to_traits_cmd="python $basedir/command_line_tools/image_to_traits.py -img $rfl_path --obs $obs_path -od output/ --topo $topo_coeffs_path --brdf $brdf_coeffs_prefix --mask --mask_threshold .3 .7  --out _topo_brdf"


echo "Executing command: $topo_brdf_coeffs_cmd"
$topo_brdf_coeffs_cmd
echo ""
echo "Executing command: $image_to_traits_cmd"
$image_to_traits_cmd
